<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '46andtwo');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wtwJ-ei/J*nf./Z7CIa?5?`x|wB&RNj$5WELD+Z8s+2 R>A;P-b93IhNLAmo`Xj5');
define('SECURE_AUTH_KEY',  '?VWmQI,#>;f./% 0 ^;]F%8#L5L)2:BI#I1FFDISy!|`EJE>RQybnu>p}wK)h{B*');
define('LOGGED_IN_KEY',    'sY&;bNG? 6fo2DB4GN8!NRh!?)+!{vQ|]DPBo%l~Zc5a/_BdU+:WL:V2#HcOBMNO');
define('NONCE_KEY',        'N*[AG/wg)g[pHUO{TQX88jsNLdDB(I$aaY[?cVlzBv+.r5<,w:|vl*(*|{qK=4id');
define('AUTH_SALT',        '.KiBwbTVJJ1N2]VO+Q;/?T*n{ YjQJ%CX&;=DABW#%>[ +Q_!*.|X@%219^^7Q 9');
define('SECURE_AUTH_SALT', '-+[V88R/btuZgNI+|y@eUxHGriFn3[gWp0H/t&~$|ImE3 H}UtsY[^i|L[hQG5Fx');
define('LOGGED_IN_SALT',   '(>AIVtOc^@cXn=i}t.Y8>uqIsN,SF}>HjTGfVRD36X[}d_N~qh;/VD~R!76WBX6V');
define('NONCE_SALT',       '+R^PiA;;;S=ab,4c-dp(Uqk({G-[I|wD;QVk>uK^a+>#F/)3--%xVQ]K>xvCO3-0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'windwaker';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
