=== HTML5 jQuery Audio Player ===
Contributors: EnigmaWeb, Base29
Donate link: http://enigmaweb.com.au
Tags: mp3 player, audio player, music player, ogg player, HTML5 audio player, mp3, podcast, jquery player
Requires at least: 3.1
Tested up to: 3.4.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Finally, a trendy looking audio player plugin. Works on all browsers including iPhone/iPad.

== Description ==

This trendy looking music player lets you add a single audio track or a full playlist to your WordPress site using shortcode. You can customise the colours of the player, and also display ratings, album cover art, and buy/download link if needed. This audio player is different from others on offer because it works on all major browsers, both PC and Mac, and on mobile devices including iPhone/iPad. Plus it looks really cool!

= Key Features =

*	Supports mp3 and ogg file formats
*   Attractive design with customisable colours
*	HTML5 based player with Flash backup 
*   Works in all major browsers - IE7, IE8, IE9, Safari, Firefox, Chrome
*   Works on mobile devices including iPhone/iPad
*   Can enable Buy or Download tracks button
*	Add the player to any post/page using shortcode `[hmp_player]`

= Demo =

[Click here for demo](http://www.galaxybrass.com.au/audio-demo/)

= Credits =

This is a WordPress version of the player created by Saleem over at [Codebase Hero](http://www.codebasehero.com/2011/06/html-music-player/) with thanks also to [Orman Clark](http://www.premiumpixels.com/freebies/compact-music-player-psd/) for the original PSD.

== Installation ==

1. Upload the `html5-jquery-audio` folder to the `/wp-content/plugins/` directory
1. Activate the HTML5 jQuery Audio Player plugin through the 'Plugins' menu in WordPress
1. Configure the plugin by going to the `HTML5 Audio Player` menu that appears in your admin menu
1. Add the player to any post/page using shortcode `[hmp_player]`

== Frequently Asked Questions ==

= I'm having problems adding mp3 files - they won't play. =

The plugin supports mp3 and ogg files. You need to upload both an mp3 and ogg version of each track in the playlist. Please also check your files are encoded correctly, and confirm that your file paths are correct. jPlayer sometimes has problems with relative urls so make sure you're using the absolute paths.

= How can I convert my files to mp3 and ogg? =

There are heaps of free conversion tools available - run a search. Personally, I use [Goldwave.](http://www.goldwave.com/)

= Does this work on all browsers? =

Yes. Its designed to work on all major browsers, both PC & Mac, and on mobile devices including iPhone/iPad.

= Is there a way to easliy replace the 'Buy' button with 'Download' button? =

Yes! Set the buy text option to �Download�, leave currency field & song price field blank, and set the buy link option on each song to the url of the mp3 or the script that initiates the download.

= Can I use this on a non-WordPress site? =

Yep, sure can. This plugin is just a WordPress version of HTML5 Music Player by Saleem over at [Codebase Hero](http://www.codebasehero.com/2011/06/html-music-player/)

= Can you help me add volume knob, mute function, remove the star rating, add my company�s logo to it, build in functionality to play videos, add a shopping cart module with credit card and PayPal checkout, add a library of 1000 albums including rights for all the music and hand over the code to me for free? =

No. Sorry.

= Where can I get support for this plugin? =

If you've tried all the obvious stuff and it's still not working please request support via the forum.


== Screenshots ==

1. An example of the inserted player
2. The display settings screen in WP-Admin

== Changelog ==

= 1.1 =
* Added sidebar with support forum, donate and wordpress.org links

= 1.0 =
* Initial release

== Upgrade Notice ==

= 1.1 =
* Added sidebar with support forum, donate and wordpress.org links

= 1.0 =
* Initial release
