<div id="sidebar">
<h2 ><?php _e('Categories'); ?></h2>
<ul >
<?php wp_list_cats('sort_column=name&optioncount=1&hierarchical=0'); ?>
</ul>
<h2 ><?php _e('Archives'); ?></h2>
<ul >
<?php wp_get_archives('type=monthly'); ?>
</ul>
<?php if(is_active_sidebar('sidebar-1')) : ?>
	<div id="widget-area" class="widget-area" role="complementary">
		<?php dynamic_sidebar('sidebar-1'); ?>
	</div>
<?php endif; ?>
</div>